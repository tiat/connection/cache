<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Cache\Adapter\Memcached;

//
use Memcached;
use Tiat\Connection\Cache\Adapter\CacheConnectionInterface;

use function array_is_list;
use function ceil;
use function count;
use function register_shutdown_function;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Connection implements CacheConnectionInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const int DEFAULT_PORT = 11211;
	
	/**
	 * @var Memcached
	 * @since   3.0.0 First time introduced.
	 */
	private Memcached $_connection;
	
	/**
	 * Class constructor
	 *
	 * @param    array    $settings    An array of settings for constructing the object
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(private readonly array $settings) {
		register_shutdown_function([$this, 'shutdown']);
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void {
		if(( $c = $this->getConnection() ) !== NULL):
			$c->quit();
		endif;
	}
	
	/**
	 * Retrieves the Memcached connection
	 *
	 * @return Memcached|null The Memcached connection or null if not set
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnection() : ?Memcached {
		//
		if(empty($this->_connection)):
			$this->_setConnection();
		endif;
		
		//
		return $this->_connection ?? NULL;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConnection() : void {
		//
		if(empty($this->_connection)):
			$this->_connection = new Memcached();
		endif;
		
		//
		if(! empty($s = $this->_getSettings())):
			// Convert array to multidimensional array (if necessary)
			$settings = $this->_getServerSettings($s);
			
			// Define the default weight if not provided in settings
			$weight = (int)ceil(100 / count($settings));
			
			//
			foreach($settings as $server):
				$this->_connection->addServer($server['host'], $server['port'] ?? self::DEFAULT_PORT,
				                              $server['weight'] ?? $weight);
			endforeach;
		endif;
	}
	
	/**
	 * Retrieves the settings
	 *
	 * @return array The settings
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getSettings() : array {
		return $this->settings;
	}
	
	/**
	 * Get the server settings
	 * If $settings is not a list, it will wrap $settings in an array.
	 *
	 * @param    array    $settings    The server settings
	 *
	 * @return array The server settings as an array
	 * @since   3.0.0 First time introduced.
	 */
	private function _getServerSettings(array $settings) : array {
		//
		if(array_is_list($settings) === FALSE):
			return [$settings];
		endif;
		
		//
		return $settings;
	}
}
