<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Cache\Adapter\Memcached;

//
use Tiat\Connection\Cache\Adapter\CacheDriverInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface MemcachedDriverInterface extends CacheDriverInterface {
	
	/**
	 * Default TTL for cache items. Default TTL is 3600 seconds (1 hour).
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const int DEFAULT_TTL = 3600;
	
	/**
	 * @param    string      $key
	 * @param    mixed       $value
	 * @param    int         $expiration
	 * @param    NULL|int    $cas
	 *
	 * @return MemcachedDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function updateItem(string $key, mixed $value, int $expiration = self::DEFAULT_TTL, ?int $cas = NULL) : MemcachedDriverInterface;
	
	/**
	 * Get the CAS value for a specific key. This is used to check if a cache item has been modified concurrently.
	 *
	 * @param    string    $key
	 *
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getItemCas(string $key) : ?int;
	
	/**
	 * Get multiple items from the Memcached driver.
	 *
	 * @param    array    $keys
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getMulti(array $keys) : ?array;
	
	/**
	 * Set multiple key-value pairs in the Memcached driver.
	 *
	 * @param    array    $keys
	 * @param    int      $expiration    The expiration time in seconds for the cached items.
	 *
	 * @return MemcachedDriverInterface The Memcached driver instance for method chaining.
	 * @since   3.0.0 First time introduced.
	 */
	public function setMulti(array $keys, int $expiration = self::DEFAULT_TTL) : MemcachedDriverInterface;
	
	/**
	 * Will return BOOL if the operation was successful. Get the delayed result with Generator with getDelayedResult()
	 * <code>
	 *     if($mc->getDelayed(['key1', 'key2'], function($value, $cas, $key)) === true):
	 *        $result = $this->getDelayedResult();
	 *     endif;
	 * </code>
	 *
	 * @param    array       $keys
	 * @param    callable    $callback
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getDelayed(array $keys, callable $callback) : bool;
	
	/**
	 * Get the delayed result from the function.
	 *
	 * @return null|array Returns null if the result is not available yet, otherwise returns the result array.
	 * @since   3.0.0 First time introduced.
	 */
	public function getDelayedResult() : ?array;
}
