<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Cache\Adapter\Memcached;

//
use Generator;
use Memcached;
use Tiat\Connection\Cache\Adapter\AbstractDriver;
use Tiat\Connection\Cache\Exception\RuntimeException;

use function extension_loaded;
use function implode;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Driver extends AbstractDriver implements MemcachedDriverInterface {
	
	/**
	 * CAS values: CAS values are used to check if a cache item has been modified concurrently.
	 *
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_cas;
	
	/**
	 * @param    Connection    $connection    The connection object to be injected.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(private readonly Connection $connection) {
		//
		if(! extension_loaded('memcached')):
			throw new RuntimeException("Memcached extension is required but the extension is not loaded.");
		endif;
	}
	
	/**
	 * @param    string    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasItem(string $key) : bool {
		//
		if(! empty($this->getItem($key))):
			return TRUE;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * Get an item from the cache.
	 *
	 * @param    string    $key
	 * @param    mixed     $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getItem(string $key, mixed $default = NULL) : mixed {
		if(( $c = $this->_getConnection() ) !== NULL):
			//
			if(empty($i = $c?->getConnection()?->get($key, NULL, Memcached::GET_EXTENDED))):
				if($c->getConnection()?->getResultCode() === Memcached::RES_NOTFOUND):
					return $default;
				else:
					$this->_writeLog($key, $c);
				endif;
			else:
				// Save the CAS value for later use
				$this->_setCas($key, $i['cas']);
			endif;
			
			//
			return $i['value'];
		else:
			$this->_throwServerException();
		endif;
	}
	
	/**
	 * Retrieve the connection object.
	 *
	 * @return null|Connection The connection object.
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConnection() : ?Connection {
		return $this->connection ?? NULL;
	}
	
	/**
	 * Write log for cache keys.
	 *
	 * @param    array|string       $keys          The keys to log.
	 * @param    null|Connection    $connection    The connection to the cache.
	 *
	 * @return void
	 * @throws RuntimeException If there is no connection or no servers for the keys.
	 * @since 3.0.0 First time introduced.
	 */
	protected function _writeLog(array|string $keys, ?Connection $connection = NULL) : void {
		if(( $c = $connection?->getConnection() ) === NULL):
			throw new RuntimeException('There is no connection at all.');
		else:
			//
			$values = implode(', ', (array)$keys);
			
			//
			if($c->getResultCode() === Memcached::RES_NO_SERVERS):
				$msg = sprintf('There are no servers for keys %s.', $values);
				throw new RuntimeException($msg);
			endif;
		endif;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _throwServerException() : void {
		throw new RuntimeException('There are no servers.');
	}
	
	/**
	 * Stores an item in the cache.
	 *
	 * @param    string    $key
	 * @param    mixed     $value
	 * @param    int       $expiration
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setItem(string $key, mixed $value, int $expiration = self::DEFAULT_TTL) : static {
		//
		if(( $c = $this->_getConnection() ) !== NULL):
			//
			if($this->getItemCas($key) === NULL):
				// Throw an exception if the connection is not available.
				if($c->getConnection()?->set($key, $value, $expiration) === FALSE):
					$this->_writeLog($key, $c);
				endif;
			else:
				// Use the CAS value to update the item.
				$this->updateItem($key, $value, $expiration, $this->_getCas($key));
			endif;
		else:
			$this->_throwServerException();
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $key
	 *
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getItemCas(string $key) : ?int {
		// Find the key at first and if it returns the value then fetch the CAS value.
		if($this->getItem($key) !== NULL):
			return $this->_getCas($key);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * Get the CAS from the cache for a specific key.
	 *
	 * @param    string    $key    The key of the item in the cache.
	 *
	 * @return int|null The CAS value for the key, or NULL if it does not exist.
	 * @since 3.0.0 First time introduced.
	 */
	protected function _getCas(string $key) : ?int {
		return $this->_cas[$key] ?? NULL;
	}
	
	/**
	 * Set the Cas value for a given key in the cache.
	 *
	 * @param    string    $key    The key of the item.
	 * @param    int       $cas    The Cas value to set.
	 *
	 * @return static
	 * @since 3.0.0 First time introduced.
	 */
	protected function _setCas(string $key, int $cas) : static {
		//
		$this->_cas[$key] = $cas;
		
		//
		return $this;
	}
	
	/**
	 * Update item with CAS or if the item doesn't exist, create it with setItem.
	 *
	 * @param    string      $key
	 * @param    mixed       $value
	 * @param    int         $expiration
	 * @param    NULL|int    $cas
	 *
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function updateItem(string $key, mixed $value, int $expiration = self::DEFAULT_TTL, ?int $cas = NULL) : static {
		//
		if($cas === NULL || $this->getItemCas($key) === NULL):
			// Item doesn't exist, so set it.
			$this->setItem($key, $value, $expiration);
		elseif(( $c = $this->_getConnection()?->getConnection() ) !== NULL):
			// Item exists, so update it with CAS.
			$c->cas($cas, $key, $value, $expiration);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Deletes an item from the cache.
	 *
	 * @param    string    $key    The key of the item to be deleted.
	 *
	 * @return   static             Returns the current object for method chaining.
	 * @since   3.0.0 First time introduced.
	 **/
	public function deleteItem(string $key) : static {
		if(( $c = $this->_getConnection() ) !== NULL):
			$c->getConnection()?->delete($key);
		else:
			$this->_throwServerException();
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $keys
	 *
	 * @return null|array
	 * @since 3.0.0 First time introduced.
	 */
	public function getMulti(array $keys) : ?array {
		if(( $c = $this->_getConnection() ) !== NULL):
			//
			if(( $result = $c->getConnection()?->getMulti($keys) ) === FALSE):
				$this->_writeLog($keys, $c);
			endif;
			
			//
			return $result;
		else:
			$this->_throwServerException();
		endif;
	}
	
	/**
	 * Set multiple items in the cache.
	 *
	 * @param    array    $keys
	 * @param    int      $expiration
	 *
	 * @return static
	 * @since 3.0.0 First time introduced.
	 */
	public function setMulti(array $keys, int $expiration = self::DEFAULT_TTL) : static {
		//
		// Handle the CAS with each value
		foreach($keys as $key => $value):
			$this->updateItem($key, $value, $expiration, $this->_getCas($key));
		endforeach;
		
		//
		return $this;
	}
	
	/**
	 * @param    array            $keys
	 * @param    NULL|callable    $callback
	 *
	 * @return bool
	 * @since 3.0.0 First time introduced.
	 */
	public function getDelayed(array $keys, ?callable $callback = NULL) : bool {
		//
		if(( $c = $this->_getConnection() ) !== NULL):
			return $c->getConnection()?->getDelayed($keys, TRUE, $callback);
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @return null|array
	 * @since 3.0.0 First time introduced.
	 */
	public function getDelayedResult() : ?array {
		//
		foreach($this->_getDelayedResult() as $value):
			$this->_setCas($value['key'], $value['cas']);
			$result[$value['key']] = $value['value'];
		endforeach;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * Get the delayed result as a generator.
	 *
	 * @return Generator
	 * @since 3.0.0 First time introduced.
	 */
	protected function _getDelayedResult() : Generator {
		if(( $c = $this->_getConnection() ) !== NULL):
			while($result = $c->getConnection()?->fetch()):
				yield $result;
			endwhile;
		endif;
	}
}
