<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Cache\Adapter;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CacheDriverInterface {
	
	/**
	 * Has an item in the cache?
	 *
	 * @param    string    $key
	 *
	 * @return bool
	 */
	public function hasItem(string $key) : bool;
	
	/**
	 * Retrieves an item from the cache.
	 *
	 * @param    string    $key
	 * @param    mixed     $default
	 *
	 * @return mixed
	 */
	public function getItem(string $key, mixed $default) : mixed;
	
	/**
	 * Sets an item in the cache.
	 *
	 * @param    string    $key
	 * @param              $value
	 * @param    int       $expiration    TTL in seconds
	 *
	 * @return CacheDriverInterface
	 */
	public function setItem(string $key, $value, int $expiration) : CacheDriverInterface;
	
	/**
	 * Deletes an item from the cache.
	 *
	 * @param    string    $key    The cache key of the item to delete.
	 *
	 * @return CacheDriverInterface The cache driver instance.
	 */
	public function deleteItem(string $key) : CacheDriverInterface;
}
