<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Cache\Adapter;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CacheConnectionInterface {
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnection() : mixed;
}
