<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Cache\Adapter;

//
use Tiat\Connection\Cache\Adapter\Memcached\Connection;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractDriver implements CacheDriverInterface {
	
	/**
	 * @return null|Connection
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _getConnection() : ?Connection;
}
