<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Cache;

//
use Tiat\Connection\Cache\Adapter\CacheDriverInterface;
use Tiat\Connection\Cache\Adapter\Memcached\Connection;
use Tiat\Connection\Cache\Adapter\Memcached\Driver;
use Tiat\Connection\Cache\Exception\InvalidArgumentException;

use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractStorage implements StorageInterface {
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private readonly string $_adapter;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private readonly array $_settings;
	
	/**
	 * @var CacheDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	private CacheDriverInterface $_driver;
	
	/**
	 * @param    string    $adapter
	 * @param    array     $settings
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(string $adapter, array $settings = []) {
		//
		$this->setAdapter($adapter);
		
		//
		if(! empty($settings)):
			$this->setSettings($settings);
		endif;
	}
	
	/**
	 * @return CacheDriverInterface The cache driver.
	 * @since   3.0.0 First time introduced.
	 */
	public function getDriver() : CacheDriverInterface {
		//
		if(empty($this->_driver)):
			$this->_createDriver();
		endif;
		
		//
		return $this->_driver;
	}
	
	/**
	 * @param    null|CacheDriverInterface    $driver
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _createDriver(?CacheDriverInterface $driver = NULL) : void {
		// Get the appropriate driver based on the adapter type
		if($driver === NULL):
			$driver = match ( $this->getAdapter() ) {
				self::ADAPTER_MEMCACHE, self::ADAPTER_MEMCACHED => new Driver(new Connection($this->getSettings())),
				default => throw new InvalidArgumentException(sprintf("Unsupported cache adapter: %s",
				                                                      $this->getAdapter()))
			};
		endif;
		
		// Set the driver instance
		if($driver !== NULL):
			$this->_driver = $driver;
		endif;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getAdapter() : string {
		return $this->_adapter;
	}
	
	/**
	 * @param    string    $adapter
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setAdapter(string $adapter) : void {
		$this->_adapter = strtolower($adapter);
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettings() : array {
		return $this->_settings;
	}
	
	/**
	 * @param    array    $settings
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function setSettings(array $settings) : void {
		$this->_settings = $settings;
	}
}
