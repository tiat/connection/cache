<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Cache
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Cache;

//
use Tiat\Connection\Cache\Adapter\CacheDriverInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface StorageInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string ADAPTER_MEMCACHED = 'memcached';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string ADAPTER_MEMCACHE = self::ADAPTER_MEMCACHED;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getAdapter() : string;
	
	/**
	 * @param    string    $adapter
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setAdapter(string $adapter) : void;
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettings() : array;
	
	/**
	 * @param    array    $settings
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function setSettings(array $settings) : void;
	
	/**
	 * Get the cache driver (same as instance).
	 * If the cache driver has not been created yet, it will be created before returning.
	 *
	 * @return CacheDriverInterface The cache driver.
	 * @since   3.0.0 First time introduced.
	 */
	public function getDriver() : CacheDriverInterface;
}
